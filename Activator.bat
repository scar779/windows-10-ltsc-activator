@echo off


:: BatchGotAdmin
:-------------------------------------
REM  --> Check for permissions
>nul 2>&1 "%SYSTEMROOT%\system32\cacls.exe" "%SYSTEMROOT%\system32\config\system"

REM --> If error flag set, we do not have admin.
if '%errorlevel%' NEQ '0' (
    echo Requesting administrative privileges...
    goto UACPrompt
) else ( goto gotAdmin )

:UACPrompt
    echo Set UAC = CreateObject^("Shell.Application"^) > "%temp%\getadmin.vbs"
    set params = %*:"=""
    echo UAC.ShellExecute "cmd.exe", "/c %~s0 %params%", "", "runas", 1 >> "%temp%\getadmin.vbs"

    "%temp%\getadmin.vbs"
    del "%temp%\getadmin.vbs"
    exit /B

:gotAdmin
    pushd "%CD%"
    CD /D "%~dp0"
:--------------------------------------
title Windows 10 LTSC Activator
PowerShell -Command "Add-Type -AssemblyName PresentationFramework;[System.Windows.MessageBox]::Show('MAKE SURE TO BE CONNECTED TO THE INTERNET BEFORE CLICKING OK!')"
move /y "csvlk-pack" "C:\Windows\System32\spp\tokens\skus" 
move /y "EnterpriseS" "C:\Windows\System32\spp\tokens\skus"
cls
cscript.exe %windir%\system32\slmgr.vbs /rilc
cscript.exe %windir%\system32\slmgr.vbs /upk >nul 2>&1
cscript.exe %windir%\system32\slmgr.vbs /ckms >nul 2>&1
cscript.exe %windir%\system32\slmgr.vbs /cpky >nul 2>&1
cscript.exe %windir%\system32\slmgr.vbs /ipk M7XTQ-FN8P6-TTKYV-9D4CC-J462D
cls
reg add "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows NT\CurrentVersion" /v CompositionEditionID /t REG_SZ /d "EnterpriseS" /f
cls
powershell -c "Invoke-WebRequest -Uri 'https://github.com/massgravel/Microsoft-Activation-Scripts/raw/master/MAS_1.4/All-In-One-Version/MAS_1.4_AIO_CRC32_9A7B5B05.cmd' -OutFile MAS1.4.cmd 
move /y MAS1.4.cmd %localappdata%\Temp
cls
echo Please wait...
timeout 5 /nobreak >nul
PowerShell -Command "Add-Type -AssemblyName PresentationFramework;[System.Windows.MessageBox]::Show('MAKE SURE TO CHOOSE HWID ACTIVATION IN THE ACTIVATOR!')"
powershell %localappdata%\Temp\./MAS1.4.cmd
pause


