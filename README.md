# Windows 10 LTSC Activator

# Table of Contents
### 1. [Info](#info-)
### 2. [HOW TO USE](#how-to-use-)
### 3. [Credits](#credits)
___________________________________________________________________________________________________________








# Info-
# **If you've ever installed Windows 10 LTSC it only allows you to use it for 90 days and later would shutdown automatically making it unusable. The script removes the 90 day evaluation and permanently activates it with HWID license!**
                                                                                                                                            

# HOW TO USE- 
# 1. [Download](https://gitlab.com/scar779/windows-10-ltsc-activator/uploads/bb6559ae2b950275b1e678ca6edcbe8d/Windows_10_LTSC_Activator.zip) the script and extract it with [7zip](https://www.7-zip.org/download.html) or any other program
# 2. Run **Activator.bat** and wait...
# 3. Choose HWID ACTIVATION
![alt text](https://i.imgur.com/EUkIlUU.png)
# 4. Windows 10 LTSC is now activated!

# Credits
# Tee Vee : [For the sku's](https://kb.teevee.asia/microsoft-windows/windows10-lstc-eval-to-ltsc-full/) 
# WindowsAddict: [For the activator in the script](https://github.com/massgravel/Microsoft-Activation-Scripts)
































